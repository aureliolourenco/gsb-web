<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>GSB - Galaxy Swiss Bourdin</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/bootstrap-grid.css" rel="stylesheet">
    <link href="/css/bootstrap-reboot.css" rel="stylesheet">
    <link href="/css/gsb.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/"><img id="logo" src="/img/gsb.png"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/">Accueil</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Prévention
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">SIDA</a>
                    <a class="dropdown-item" href="#">Hépatite B</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Recherches
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">SIDA</a>
                    <a class="dropdown-item" href="#">Hépatite B</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/">Don</a>
            </li>
        </ul>
        <button id="btnConnexion" class="btn btn-secondary">Connexion</button>
    </div>
</nav>
<div class="row">
    <div class="col-2" id="presentation">
        Présentation
    </div>
</div>
<div class="row">
    <div class="col-12" id="presentation-text">
        Texte de présentation
    </div>
</div>
</body>
</html>
